// extern modules
var Q = require('q');
var mkdirp = require('mkdirp');
var fs = require('fs');
// intern modules
var utils = require('./utils');
var props = require('./props');

/**
 * Prepares the conf/server.xml file.
 * @param options
 * @returns {*|promise}
 */
var prepareServerXml = function(options) {
    var deferred = Q.defer();
    var version = options.version;
    var connectorPort = options.port;
    var context = options.contextPath;

    var serverPort = -1;
    utils.getOpenServerPorts().then(function(ports) {
        if (ports.length > 0) {
            serverPort = ports[0];
        }
    });

    var redirectPort = -1;
    utils.getOpenRedirectPorts().then(function(ports) {
        if (ports.length > 0) {
            redirectPort = ports[0];
        }
    });

    var confDir = utils.getConfDir(version);
    var serverXmlFile = confDir + '/conf/server.xml';

    // replace server port
    utils.replaceSync(serverXmlFile, {
        serverPort: [/(<Server[\s\S]*)port="[0-9]*"(.*(\/>|>))/, '$1port="' + serverPort + '"$2'],
        connectorPort: [/(<Connector.*)port="[0-9]*"([\s\S]*(\/>|>))/, '$1port="' + connectorPort + '"$2'],
        redirectPort: [/(<Connector[\s\S]*)redirectPort="[0-9]*"([\s\S]*(\/>|>))/, '$1redirectPort="' + redirectPort + '"$2'],
        contextPath: [/(<Context[\s\S]*)path="(\/[a-zA-Z0-9]*)*"([\s\S]*(\/>|>))/, '$1path="' + context + '"$3']
    });

    console.log('### Updated server.xml');

    deferred.resolve();

    return deferred.promise;
};


/**
 * Prepares the confluence-init.properties to set the correct path for the Confluence home directory.
 * @param options
 * @returns {*|promise}
 */
var prepareConfHome = function(options) {
    var deferred = Q.defer();
    var homeDir = utils.getConfHome(options.version);

    mkdirp(homeDir, function(error) {
        if (error) {
            deferred.reject(error);
        } else {
            // set path to init properties
            var initProps = utils.getConfDir(options.version) + '/confluence/WEB-INF/classes/confluence-init.properties';

            utils.replaceSync(initProps, {
                homeDir: [/# confluence\.home=c:\/confluence\/data/g, 'confluence.home=' + homeDir.replace(/\\/g, '/')]
            });

            console.log('### Configured Confluence home directory: ', homeDir);
            deferred.resolve();
        }
    });

    return deferred.promise;
};


/**
 * Prepares the bin/setenv.[sh|bat] file by adding some more options to $CATALINA_OPTS.
 * @param options
 * @returns {*|promise}
 */
var prepareSetenv = function(options) {
    var devOptions = options.options;
    var deferred = Q.defer();
    var setenvFile = utils.getConfDir(options.version) + '/bin/setenv' + utils.getScriptFiletype();
    var debugging = '-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=' + options.debugPort;
    var batching = '-Dconfluence.context.batching.disable=true';
    var devMode = '-Dconfluence.dev.mode=true -Dconfluence.devmode=true';
    var strictMode = '-Dconfluence.velocity.deprecation.strictmode=false';
    var disableCaches = '-Datlassian.disable.caches=true';
    var minification = '-Datlassian.webresource.disable.minification=true';

    var opts = 'CATALINA_OPTS="${CATALINA_OPTS}';
    if (options.debugPort) {
        opts += ' ' + debugging;
    }

    if (!devOptions.batching) {
        opts += ' ' + batching;
    }
    if (!devOptions.devMode) {
        opts += ' ' + devMode;
    }
    if (!devOptions.strict) {
        opts += ' ' + strictMode;
    }
    if (!devOptions.caches) {
        opts += ' ' + disableCaches;
    }
    if (!devOptions.min) {
        opts += ' ' + minification;
    }
    props.getOptions().then(function(data) {
        opts += ' ' + data;
        opts += '"';

        utils.replaceSync(setenvFile, {
            logFile: [/-Xloggc:.*\.log/, '-Xloggc:$LOGBASEABS/logs/gc-`date +%F_%H-%M-%S`.log'],
            opts: [/CATALINA_OPTS="\$\{CATALINA_OPTS\}.*export CATALINA_OPTS|export CATALINA_OPTS/, opts + ' export CATALINA_OPTS']
        });

        console.log('### Configured environment variables.');
        deferred.resolve();
    });

    return deferred.promise;
};

/**
 *
 * @param options contains:
 * version: Confluence version
 * port: which port to run Confluence on - default 1990
 * contextPath: which context path to use for confluence - default '/confluence'
 * @returns {*} a promise and resolves if all preparations are done
 */
exports.prepare = function(options) {
    var deferred = Q.defer();

    if (!(options && options.version)) {
        deferred.reject({
            message: 'Not all necessary options specified.'
        });
    } else {
        console.log('### Start to prepare the Confluence standalone...');

        Q.all([prepareServerXml(options), prepareConfHome(options), prepareSetenv(options)]).then(function() {
            console.log('### Preparation complete!');
            deferred.resolve();
        }, function(error) {
            // for possible errors
            console.log('### Preparation failed!');
            deferred.reject(error);
        });
    }

    return deferred.promise;
};
