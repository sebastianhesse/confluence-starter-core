var starter = require('./start');
var download = require('./download');
var prepare = require('./prepare');
var lister = require('./list');
var cleaner = require('./clean');
var props = require('./props');

// starter methods
exports.startInstance = starter.startInstance;

// download methods
exports.downloadInstance = download.run;

// prepare methods
exports.prepareInstance = prepare.prepare;

// lister methods
exports.getVersions = lister.getVersions;
exports.listVersions = lister.listVersions;

// cleaner methods
exports.emptyHomeFolder = cleaner.emptyHomeFolder;

//props methods
exports.addOption = props.addOption;
exports.getOptions = props.getOptions;
exports.resetOptions = props.resetOptions;
exports.setOptions = props.setOptions;
exports.showOptions = props.showOptions;

