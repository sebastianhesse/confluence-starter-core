// extern modules
var spawn = require('child_process').spawn;
// intern modules
var downloader = require('./download');
var prepareConf = require('./prepare');
var utils = require('./utils');
// vars
var confInstance = null;


/**
 * Starts a Confluence instance with given version and options.
 * @param version a Confluence version as a string, e.g. 5.9.6
 * @param options object containing information like port, context path, debug port and others
 */
var startInstance = function(version, options) {
    var port = getPort(options);
    var contextPath = getContextPath(options);
    var debugPort = getDebugPort(options);

    console.log('### Using properties version=' + version + ', port=' + port + ', contextPath=' + contextPath + ', debugPort=' + debugPort);

    registerQuitActions();
    downloadAndStart(version, port, contextPath, debugPort, options);
};


/**
 * Gets the port from the options. Default is 1990.
 * @param options
 * @returns {number}
 */
var getPort = function(options) {
    var port = 1990;

    if (utils.isNotEmptyString(options.port)) {
        port = parseInt(options.port);
    } else {
        utils.getOpenConnectorPorts().then(function(ports) {
            if (ports.length > 0) {
                port = ports[0];
            }
        });
    }

    return port;
};


/**
 * Gets the context path from the options. Default is /confluence.
 * @param options
 * @returns {string}
 */
var getContextPath = function(options) {
    var contextPath = '/confluence';
    if (utils.isNotEmptyString(options.context)) {
        contextPath = utils.getPathWithSlash(options.context);
    }
    return contextPath;
};


/**
 * Gets the debug port from the options. No default port specified.
 * @param options
 * @returns {*}
 */
var getDebugPort = function(options) {
    var debugPort = null;
    if (utils.isNotEmptyString(options.debug)) {
        debugPort = parseInt(options.debug);
    }
    return debugPort;
};


/**
 * Registers handlers to listen for the "Quit Event", i.e. if the users presses Strg+C. Then the process of the Confluence instance 
 * gets killed.
 */
var registerQuitActions = function() {
    // http://stackoverflow.com/questions/6958780/quitting-node-js-gracefully
    // http://stackoverflow.com/questions/10021373/what-is-the-windows-equivalent-of-process-onsigint-in-node-js
    if (process.platform == 'win32') {
        var rl = require("readline").createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.on("SIGINT", function() {
            process.emit("SIGINT");
        });
    }

    process.on('SIGINT', function() {
        if (confInstance && confInstance.pid) {
            console.log('### Stopping Confluence with PID=' + confInstance.pid);

            if (process.platform == 'win32') {
                spawn("taskkill", ["/pid", confInstance.pid, '/f', '/t']);
            } else {
                // TODO: improve killing the process
                //confInstance.emit('SIGINT');
                confInstance.kill('SIGTERM');
                //process.kill(confInstance.pid, 'SIGTERM');
            }

            console.log('### Stopped Confluence.');
        }

        process.exit();
    });
};


/**
 * Starts to download Confluence in given version, initiates a preparation of Confluence configuration (e.g. set environment variables or 
 * ports) and initiates a start of the instance.
 * @param version
 * @param port
 * @param contextPath
 * @param debugPort
 * @param options
 */
var downloadAndStart = function(version, port, contextPath, debugPort, options) {
    downloader.run(version).then(function(response) {
        console.log('### Using Confluence from ' + utils.getTargetDir(version));

        prepareConf.prepare({
            version: version,
            port: port,
            contextPath: contextPath,
            debugPort: debugPort,
            options: options
        }).then(function(prepareResponse) {
            confInstance = start(utils.getConfDir(version));
        }, function(error) {
            console.log('### Failed to prepare the Confluence standalone.');
            console.log(error);
        });
    }, function(error) {
        console.log('### Failed to download Confluence.');
        console.log(error);
    });
};


/**
 * Starts the Confluence instance by executing the start-confluence.[sh|bat] script and stores process information.
 * @param confDir
 */
var start = function(confDir) {
    console.log('### Starting confluence now...');

    var startupScript = confDir + '/bin/start-confluence';
    startupScript += process.platform == 'win32' ? '.bat' : '.sh';
    var arguments = [];
    arguments.push(process.platform == 'win32' ? '/fg' : '-fg');
    var cwd = confDir;

    if (process.platform == 'win32') {
        startupScript = startupScript.replace(/\//g, '\\');
        cwd = cwd.replace(/\//g, '\\');
    }

    confInstance = spawn(startupScript, arguments, {
        detached: false,
        cwd: cwd,
        stdio: 'inherit'
    });

    console.log('### Started Confluence instance with pid=' + confInstance.pid);
};

exports.startInstance = startInstance;
