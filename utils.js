var getPorts = require('get-ports');
var Q = require('q');
var fs = require('fs-extra');

/**
 * These file contains helper methods to retrieve the correct paths, directories and URLs to download and navigate to Confluence instances.
 * Each instance will be stored in #getConfStarterDir(). Example directory structure:
 * confluence-starter
 * -- 5.0.0
 * -- -- atlassian-confluence-5.0.0
 * -- -- -- bin
 * -- -- -- conf
 * -- -- -- ...
 * -- -- home
 * -- 5.0.1
 * ...
 */

/**
 * @returns {string} the root directory of confluence-starter where all instances will be stored
 */
var getConfStarterDir = function() {
    var userDir = process.env[(process.platform == 'win32' ? 'USERPROFILE' : 'HOME')];
    return userDir + '/confluence-starter';
};

/**
 * @param version Confluence version
 * @returns {string} the root directory for a downloaded Confluence instance
 */
var getTargetDir = function(version) {
    return getConfStarterDir() + '/' + version;
};

/**
 * @param version
 * @returns {string}
 */
var getConfDir = function(version) {
    return getTargetDir(version) + '/' + getDefaultArchiveFilename(version);
};

/**
 * @param version Confluence version
 * @returns {string} the path to the home directory of the Confluence instance for given version
 */
var getConfHome = function(version) {
    return getTargetDir(version) + '/home';
};

/**
 * @param version Confluence version
 * @returns {string} the archive filename based on the version, but without the file type
 */
var getDefaultArchiveFilename = function(version) {
    return 'atlassian-confluence-' + version;
};

/**
 * @returns {string} '.zip' which is the only supported format for now
 */
var getDefaultArchiveFileType = function() {
    return '.zip';
};

/**
 * @param version Confluence version
 * @returns {string} the download URL for the Confluence version
 */
var getDefaultDownloadUrl = function(version) {
    return 'https://www.atlassian.com/software/confluence/downloads/binary/'
        + getDefaultArchiveFilename(version) + getDefaultArchiveFileType();
};

/**
 * @param path any path
 * @returns {string} the path with a leading '/' (Unix) or '\' (Windows) as the first character
 */
var getPathWithSlash = function(path) {
    if (process.env.platform == 'win32') {
        return path.indexOf('\\') == 0 ? path : '\\' + path;
    } else {
        return path.indexOf('/') == 0 ? path : '/' + path;
    }
};

/**
 * Finds open ports for the connector.
 * @returns {*} a promise which will be resolved if open ports have been found
 */
var getOpenConnectorPorts = function() {
    return getOpenPorts(1990, 2989);
};

/**
 * Finds open ports for server.
 * @returns {*} a promise which will be resolved if open ports have been found
 */
var getOpenServerPorts = function() {
    return getOpenPorts(8000, 8099);
};

/**
 * Finds open ports for server. Only necessary when https is used.
 * @returns {*} a promise which will be resolved if open ports have been found
 */
var getOpenRedirectPorts = function() {
    return getOpenPorts(8400, 8499);
};

var getOpenPorts = function(start, end) {
    var deferred = Q.defer();

    getPorts([start, end], function(error, ports) {
        if (error) {
            deferred.reject(error);
        } else {
            deferred.resolve(ports);
        }
    });

    return deferred.promise;
};

var getScriptFileType = function() {
    return process.env.platform == 'win32' ? '.bat' : '.sh';
};

var isNotEmptyString = function(text) {
    return typeof text == 'string' && text.length > 0;
};

/**
 * Replaces parts of a file synchronously. Searches for each replacement key in the file and replaces it with its corresponding value.
 * @param file absolute path of the file
 * @param replacements a key value object: key is replaced by its corresponding value. 
 */
var replaceSync = function(file, replacements) {
    var content = fs.readFileSync(file, 'utf8');
    var r = replacements;

    for (var key in r) {
        if (r.hasOwnProperty(key)) {
            if (!r[key] || r[key].length != 2) {
                console.log('Illegal definition of arguments for replacing contents.');
            } else {
                content = content.replace(r[key][0], r[key][1]);
            }
        }
    }

    fs.writeFileSync(file, content, 'utf8');
};


var getPropsFilePath = function() {
    return getConfStarterDir() + '/.env-vars';
};

exports.getConfStarterDir = getConfStarterDir;
exports.getTargetDir = getTargetDir;
exports.getConfDir = getConfDir;
exports.getConfHome = getConfHome;
exports.getDefaultArchiveFilename = getDefaultArchiveFilename;
exports.getDefaultArchiveFileType = getDefaultArchiveFileType;
exports.getDefaultDownloadUrl = getDefaultDownloadUrl;
exports.getPathWithSlash = getPathWithSlash;
exports.getOpenServerPorts = getOpenServerPorts;
exports.getOpenConnectorPorts = getOpenConnectorPorts;
exports.getOpenRedirectPorts = getOpenRedirectPorts;
exports.getScriptFiletype = getScriptFileType;
exports.isNotEmptyString = isNotEmptyString;
exports.replaceSync = replaceSync;
exports.getPropsFilePath = getPropsFilePath;