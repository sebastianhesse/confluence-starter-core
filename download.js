// extern modules
var Download = require('download');
var Decompress = require('decompress');
var downloadStatus = require('download-status');
var wrench = require('wrench');
var fs = require('fs');
var Q = require('q');
// intern modules
var utils = require('./utils');

exports.run = function(version) {
    if (!version) {
        console.log('### No version specified for Confluence.');
        return;
    }

    var deferred = Q.defer();
    var downloadUrl = utils.getDefaultDownloadUrl(version);
    var targetDir = utils.getTargetDir(version);
    var filename = utils.getDefaultArchiveFilename(version);
    var filetype = utils.getDefaultArchiveFileType();

    // Check if Confluence instance was already downloaded, if yes, then skip the download
    fs.access(targetDir, fs.F_OK, function(err) {
        if (!err) {
            // target directory of Confluence already exists
            console.log('### Confluence already downloaded.');

            deferred.resolve({
                targetRoot: targetDir,
                confDir: filename
            });
        } else {
            // Confluence in desired version is not downloaded yet
            console.log('### Need to download Confluence in desired version...');

            downloadAndExtract(downloadUrl, targetDir, filename, filetype, deferred);
        }
    });

    return deferred.promise;
};

var downloadAndExtract = function(downloadUrl, targetDir, filename, filetype, deferred) {
    new Download({})
        .get(downloadUrl)
        .dest(targetDir)
        .use(downloadStatus())
        .run(function(error, files) {
            if (error) {
                deferred.reject(error);
            } else {
                if (files) {
                    console.log('### Download complete. Start to extract files...');

                    var zipPath = targetDir + '/' + filename + filetype;
                    extractZip(targetDir, zipPath, filename, deferred);
                }
            }
        });
};

var extractZip = function(targetDir, zipPath, filename, deferred) {
    new Decompress({
        mode: '777'
    }).src(zipPath)
        .dest(targetDir)
        .use(Decompress.zip())
        .run(function(error, files) {
            if (error) {
                deferred.reject(error);
            } else {
                if (files) {
                    console.log('### Successfully extracted files.');

                    // set chmod manually, because the decompressor is buggy
                    wrench.chmodSyncRecursive(targetDir, 0777);
                    deferred.resolve({
                        targetRoot: targetDir,
                        confDir: filename
                    });
                }
            }
        });
};
