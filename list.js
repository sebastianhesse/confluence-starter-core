var fs = require('fs');
var Q = require('q');
var utils = require('./utils');

/**
 * Retrieves the downloaded Confluence versions and prints them on the console.
 */
var listVersions = function() {
    var versions = getVersions();

    var output = 'Downloaded Confluence versions: ';
    if (!versions) {
        output = 'No downloaded versions yet.';
    } else {
        versions.forEach(function(value, index) {
            output += value;
            if (index < versions.length - 1) {
                output += ', ';
            }
        });
    }

    console.log('### ' + output);
};


/**
 * Gets a list of available Confluence instances (i.e. folders) which have been downloaded to the confluence-starter directory.
 * Ignores folders beginning with a ".", but might contain some other folders, so use it carefully.
 * 
 * @returns {Array.<T>|*}
 */
var getVersions = function() {
    return fs.readdirSync(utils.getConfStarterDir()).filter(function(element) {
        // ignore folders like .DS_STORE etc.
        if (element.indexOf('.') != 0) {
            return element;
        } else {
            return false;
        }
    });
};

exports.listVersions = listVersions;
exports.getVersions = getVersions;