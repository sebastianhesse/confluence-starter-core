var fs = require('fs-extra');
var Q = require('q');
var utils = require('./utils');
var propsFile = utils.getPropsFilePath();

var addOption = function(option) {
    if (!option) {
        console.log('### Please use a valid value as option!');
        return;
    }
    fs.ensureFile(propsFile, function(error) {
        if (error) {
            printReadError(error);
        } else {
            fs.readFile(propsFile, 'utf8', function(readErr, data) {
                if (readErr) {
                    printReadError(readErr);
                } else {
                    var updatedData = data + ' -D' + option;
                    fs.outputFile(propsFile, updatedData, function(writeErr) {
                        if (writeErr) {
                            printWriteError(writeErr);
                        } else {
                            console.log('### Successfully updated properties.');
                            console.log('### Stored properties: ' + updatedData);
                        }
                    });
                }
            });
        }
    });
};


var getOptions = function() {
    var deferred = Q.defer();
    fs.ensureFile(propsFile, function(error) {
        if (error) {
            printReadError(error)
        } else {
            fs.readFile(propsFile, 'utf8', function(readErr, data) {
                if (readErr) {
                    printReadError(readErr);
                } else {
                    deferred.resolve(data);
                }
            });
        }
    });
    return deferred.promise;
};


var showOptions = function() {
    getOptions().then(function(options) {
        console.log('### Stored properties: ' + options);
    });
};


var resetOptions = function() {
    setOptions('');
};


var setOptions = function(options) {
    fs.ensureFile(propsFile, function(error) {
        if (error) {
            printReadError(error);
        } else {
            fs.outputFile(propsFile, options, function(writeErr) {
                if (writeErr) {
                    printWriteError(writeErr);
                } else {
                    console.log('### Successfully updated properties.');
                    console.log('### Stored properties: ' + options);
                }
            });
        }
        
    });
};

function printReadError(readErr) {
    console.log('### Error while reading file: ', readErr);
}

function printWriteError(writeErr) {
    console.log('### Error while writing file: ', writeErr);
}

exports.addOption = addOption;
exports.getOptions = getOptions;
exports.showOptions = showOptions;
exports.resetOptions = resetOptions;
exports.setOptions = setOptions;