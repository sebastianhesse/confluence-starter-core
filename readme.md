# confluence-starter-core
Package containing the core libraries for confluence-starter (the command line).

## Author
Sebastian Hesse

## How to use
1. Install: ```npm install --save confluence-starter-core```
2. Use the main module: ```var core = require('confluence-starter-core')```
3. Or use a submodule directly: ```var starter = require('confluence-starter-core/start)```
4. Available submodules: ```prepare, download, start, clean, list, props, utils```

Please take a look at ```index.js``` for the provided methods (or directly at the submodules).

## License
The MIT License (MIT)

Copyright (c) 2016 Sebastian Hesse

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
