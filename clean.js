var fs = require('fs-extra');
var Q = require('q');
var utils = require('./utils');


/**
 * Makes the home folder for given version empty.
 * @param version
 */
var emptyHomeFolder = function(version) {
    var confHome = utils.getConfHome(version);

    // check that directory really exists
    fs.access(confHome, fs.F_OK, function(err) {
        if (!err) {
            // directory exists, thus remove all content from it
            fs.emptyDir(confHome, function(error) {
                if (!error) {
                    console.log('### Successfully cleaned home directory under ' + confHome);
                } else {
                    console.log('### Could not clean home directory under ' + confHome + '. ' +
                        'Please make sure that you\'ve selected the correct version, Confluence is not running and ' +
                        'nothing else is using files from the directory.');
                }
            });
        } else {
            console.log('### Could not clean home directory under ' + confHome + '. ' +
                'Please make sure that you\'ve selected the correct version, Confluence is not running and ' +
                'nothing else is using files from the directory.');
        }
    });
};

exports.emptyHomeFolder = emptyHomeFolder;